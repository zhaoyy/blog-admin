/**
 * Created by Xulu on 2014/6/11.
 */
angular.module('phonecatFilters', []).filter('checkmark', function(){
    return function (input) {
        return input ? '\u2713' : '\u2718';
    };
});