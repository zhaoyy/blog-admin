var TableEditable = function () {

    return {

        //main function to initiate the module
	    init: function () {
	        var nCol = ['p.product_id','p.price','p.quantity','p.amount','p.unit','p.origin','p.warehouse_id','p.status','p.delivery_address','p.delivery_phone','p.remark'];
	        $('#sample_editable_1_new').click(function (e) {
	              e.preventDefault();
	              innerHTML = '<tr>';
	              innerHTML += $('#purchase_id').html();  //父ID
	              innerHTML += '<input type="hidden" name="p.id" value="">';
	              innerHTML += '<td name="p.product_id"><select required style="width: 120px;height:25px" name="p.product_id">';
	              innerHTML += $('#products').html();
	              innerHTML += '</select></td>';
	              innerHTML += '<td name="p.price"><input required type="text" name="p.price" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.quantity"><input required type="text" name="p.quantity" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.amount" class="amount"><input readonly type="text" name="p.amount" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.unit"><input type="text" name="p.unit" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.origin"><input type="text" name="p.origin" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.warehouse_id"><select required name="p.warehouse_id" style="width: 120px;height:25px">';
	              innerHTML += $('#warehouses').html();
	              innerHTML += '</select></td>';
	              innerHTML += '<td name="p.status"><input type="text" name="p.status" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.delivery_address"><input type="text" name="p.delivery_address" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.delivery_phone"><input type="text" name="p.delivery_phone" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.remark"><input type="text" name="p.remark" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td style="width:13%"><a href="javascript:;" class="btn mini blue-stripe save">保存</a> <a href="/purchase_detail/delete/${p.id}" class="btn mini red-stripe cancel">取消</a></td></tr>';
	              $("#sample_editable_1 > tbody").append(innerHTML);
	        });
	            
	      $('#sample_editable_1 a.delete').live('click', function (e) {
	          e.preventDefault();
	          $('#detail_form').validate();
	          if (confirm("确定要删除这一行吗?") == false) {
	              return;
	          }
	          var url = $(this).parent().parent().find('td:last .delete').attr('href');
	          $.post(url);
	          
	          $(this).parent().parent().remove();
	          
	          sumamount();
	          
	      });
	
	      $('#sample_editable_1 a.cancel').live('click', function (e) {
	          e.preventDefault();
	          var id = $(this).parent().parent().find('input[name="p.id"]').val();
	          if (id == "" || id == null) {
	        	  $(this).parent().parent().remove();
	          } else {
	        	  
	          }
	          
	      });
	
	      $('#sample_editable_1 a.edit').live('click', function (e) {
	          e.preventDefault();
	          for(var i=0;i<nCol.length;i++) {
	        	  var ntd = $(this).parent().parent().find('td[name="'+nCol[i]+'"]');
	        	  ntd.html('<input type="text" name="'+nCol[i]+'" style="width: 80%;height:15px" value="'+ntd.html()+'">');
	          }
	          
	          $(this).parent().parent().find('td:last .edit').text("保存");
	          $(this).parent().parent().find('td:last .edit').addClass('save').removeClass('edit');
//	          $(this).parent().parent().find('td:last .delete').text("取消");
//	          $(this).parent().parent().find('td:last .delete').addClass('cancel').removeClass('delete');
	      });
	      
	      $('#sample_editable_1 a.save').live('click', function (e) {
	          e.preventDefault();
	          
	          var url = "/purchase_detail/save";
	          $.post(url ,$(this).parent().parent().find('select,input').serialize(), function(data) {
	        	  $("#purchase_detail_ids").val(data.id + "," + $("#purchase_detail_ids").val()) ;
	        	  
	          });
	          
	          for(var i=0;i<nCol.length;i++) {
	        	  var ntd = $(this).parent().parent().find('td[name="'+nCol[i]+'"]');
	        	  ntd.html(ntd.find('*[name="'+nCol[i]+'"]').val());
	          }
	          var ntd = $(this).parent().parent().find('td[name="p.product_id"]');
	          ntd.html($('#products > option[value="'+ntd.html()+'"]').html());
	          
	          var ntd = $(this).parent().parent().find('td[name="p.warehouse_id"]');
	          ntd.html($('#warehouses > option[value="'+ntd.html()+'"]').html());
	          
	          sumamount();
	          
	          $(this).parent().parent().find('td:last .save').text("编辑");
	          $(this).parent().parent().find('td:last .save').addClass('edit').removeClass('save');
	          $(this).parent().parent().find('td:last .cancel').text("删除");
	          $(this).parent().parent().find('td:last .cancel').addClass('delete').removeClass('cancel');
	      });
	      
	      $('#sample_editable_1 input[name="p.price"],input[name="p.quantity"]').live('blur', function (e) {
	    	  
	          var parent = $(this).parent().parent();
	          parent.find('input[name="p.amount"]').val(parseFloat(parent.find('input[name="p.price"]').val()) * parseFloat(parent.find('input[name="p.quantity"]').val()));
	          
	          
	      });
	      
	      var sumamount = function (){
	    	  var sum = 0;
	    	  $('#sample_editable_1').find('input[name="p.amount"]').each(function() {
	        	  sum = parseFloat(sum) + parseFloat($(this).val());
	          });
	          $('#sample_editable_1').find('td.amount').each(function() {
	        	  sum = parseFloat(sum) + parseFloat($(this).html());
	          });
	          $('#amount').val(parseFloat(sum)); 
	      };
	      
	   }

    };

}();


var TableEditableSale = function () {

    return {

        //main function to initiate the module
	    init: function () {
	        var nCol = ['p.purchase_no','p.product_id','p.warehouse_id','p.price','p.quantity','p.amount','p.status','p.delivery_address','p.car_no','p.freight','p.trans_company','p.delivery_date','p.other_amount','p.remark'];
	        $('#sample_editable_2_new').click(function (e) {
	              e.preventDefault();
	              innerHTML = '<tr>';
	              innerHTML += $('#sale_id').html();  //父ID
	              innerHTML += '<input type="hidden" name="p.id" value="">';
	              innerHTML += '<td name="p.purchase_no"><select style="width: 120px;height:25px" name="p.purchase_no">';
	              innerHTML += $('#purchases').html();
	              innerHTML += '</select></td>';
	              innerHTML += '<td name="p.product_id"><select style="width: 120px;height:25px" name="p.product_id">';
	              innerHTML += $('#products').html();
	              innerHTML += '</select></td>';
	              innerHTML += '<td name="p.warehouse_id"><select name="p.warehouse_id" style="width: 120px;height:25px">';
	              innerHTML += $('#warehouses').html();
	              innerHTML += '</select></td>';
	              innerHTML += '<td name="p.price"><input type="text" name="p.price" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.quantity"><input type="text" name="p.quantity" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.amount" class="amount"><input readonly type="text" name="p.amount" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.status"><input type="text" name="p.status" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.delivery_address"><input type="text" name="p.delivery_address" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.car_no"><input type="text" name="p.car_no" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.freight"><input type="text" name="p.freight" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.trans_company"><input type="text" name="p.trans_company" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.delivery_date"><input type="text" name="p.delivery_date" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td name="p.other_amount"><input type="text" name="p.other_amount" style="width: 80%;height:15px" value=""></td>';	              
	              innerHTML += '<td name="p.remark"><input type="text" name="p.remark" style="width: 80%;height:15px" value=""></td>';
	              innerHTML += '<td style="width:13%"><a href="javascript:;" class="btn mini blue-stripe save">保存</a> <a href="/purchase_detail/delete/${p.id}" class="btn mini red-stripe cancel">取消</a></td></tr>';
	              $("#sample_editable_2 > tbody").append(innerHTML);
	        });
          $('#sample_editable_2 select[name="p.purchase_no"]').live('change', function (e) {
	          e.preventDefault();
	          $(this).parent().parent().find('select,input').serialize();
	          var parent = $(this).parent().parent();
	          var no = $(this).val();
	          var url = "/stock/get_by_no";
	          var product = "";
	          var warehouse = "";
	          $.post(url ,{"no":no}, function(data) {
	        	  $.each(data.products, function(index, item) {
	        		  product += "<option value='" + item.product_id + "'>" + item.product_name + "</option>";
	        	  });
	        	  $.each(data.warehouses, function(index, item) {
	        		  warehouse += "<option value='" + item.warehouse_id + "'>" + item.warehouse_name + "</option>";
	        	  });
	        	  parent.find('select[name="p.product_id"]').html(product);
	        	  parent.find('select[name="p.warehouse_id"]').html(warehouse);
	          });
	          
	      });	       
	      $('#sample_editable_2 a.delete').live('click', function (e) {
	          e.preventDefault();
	
	          if (confirm("确定要删除这一行吗?") == false) {
	              return;
	          }
	          var url = $(this).parent().parent().find('td:last .delete').attr('href');
	          $.post(url);
	          
	          $(this).parent().parent().remove();
	          
	          sumamount2();
	          
	      });
	
	      $('#sample_editable_2 a.cancel').live('click', function (e) {
	          e.preventDefault();
	          var id = $(this).parent().parent().find('input[name="p.id"]').val();
	          if (id == "" || id == null) {
	        	  $(this).parent().parent().remove();
	          } else {
	        	  
	          }
	          
	      });
	
	      $('#sample_editable_2 a.edit').live('click', function (e) {
	          e.preventDefault();
	          for(var i=0;i<nCol.length;i++) {
	        	  var ntd = $(this).parent().parent().find('td[name="'+nCol[i]+'"]');
	        	  ntd.html('<input type="text" name="'+nCol[i]+'" style="width: 80%;height:15px" value="'+ntd.html()+'">');
	          }
	          
	          $(this).parent().parent().find('td:last .edit').text("保存");
	          $(this).parent().parent().find('td:last .edit').addClass('save').removeClass('edit');

	      });
	      
	      $('#sample_editable_2 a.save').live('click', function (e) {
	          e.preventDefault();
	          var url = "/sale_detail/save";
	          $.post(url ,$(this).parent().parent().find('select,input').serialize(), function(data) {
	        	  $("#sale_detail_ids").val(data.id + "," + $("#sale_detail_ids").val()) ;
	          });
	          
	          for(var i=0;i<nCol.length;i++) {
	        	  var ntd = $(this).parent().parent().find('td[name="'+nCol[i]+'"]');
	        	  ntd.html(ntd.find('*[name="'+nCol[i]+'"]').val());
	          }
	          var ntd = $(this).parent().parent().find('td[name="p.product_id"]');
	          ntd.html($('#products > option[value="'+ntd.html()+'"]').html());
	          
	          var ntd = $(this).parent().parent().find('td[name="p.warehouse_id"]');
	          ntd.html($('#warehouses > option[value="'+ntd.html()+'"]').html());
	          
	          sumamount2();
	          
	          $(this).parent().parent().find('td:last .save').text("编辑");
	          $(this).parent().parent().find('td:last .save').addClass('edit').removeClass('save');
	          $(this).parent().parent().find('td:last .cancel').text("删除");
	          $(this).parent().parent().find('td:last .cancel').addClass('delete').removeClass('cancel');
	      });
	      
	      $('#sample_editable_2 input[name="p.price"],input[name="p.quantity"]').live('blur', function (e) {
	    	  
	          var parent = $(this).parent().parent();
	          parent.find('input[name="p.amount"]').val(parseFloat(parent.find('input[name="p.price"]').val()) * parseFloat(parent.find('input[name="p.quantity"]').val()));
	          
	          
	      });
	      
	      var sumamount2 = function (){
	    	  var sum = 0;
//	    	  $('#sample_editable_2').find('input[name="p.amount"]').each(function() {
//	        	  sum = parseFloat(sum) + parseFloat($(this).val());
//	          });
	          $('#sample_editable_2').find('td.amount').each(function() {
	        	  sum = parseFloat(sum) + parseFloat($(this).html());
	          });
	          $('#amount').val(parseFloat(sum)); 
	      };
	      
	   }

    };

}();