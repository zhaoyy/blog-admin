package cc.xulu.model;

import java.util.List;

import com.jfinal.plugin.activerecord.Model; 

public class Comment extends Model<Comment>{

    private static final long serialVersionUID = 1L;
    public static final Comment dao = new Comment();
    
    public Post getPost() {
    	return Post.dao.findById(get("post_id"));
    }
    public User getUser() {
    	return User.dao.findById(get("user_id"));
    }
 }