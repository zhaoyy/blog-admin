package cc.xulu.controller;

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import cc.xulu.model.*;

public class PostController extends Controller {
	public void index() {
		setAttr("postPage", Post.dao.paginate(getParaToInt(0, 1), 10, "select *", "from post order by id asc"));
		render("list.html");
	}
	
	public void add() {
	    setAttr("category", Category.dao.find("select * from category"));
	    setAttr("user", User.dao.find("select * from user"));
	    render("edit.html");
	}

    public void edit() {
        setAttr("category", Category.dao.find("select * from category"));
        setAttr("user", User.dao.find("select * from user"));
        setAttr("post", Post.dao.findById(getParaToInt()));
        render("edit.html");
    }
	
	public void save() {
		Post post = getModel(Post.class);

		post.save();

        renderJson(post);
	}

    public void show() {
        Post post = Post.dao.findById(getParaToInt());

        renderJson(post);

    }
	
	public void update() {
        Post post = getModel(Post.class);
        post.update();
        renderJson(new Status());
	}
	
	public void delete() {
        Post.dao.deleteById(getParaToInt());
        renderJson(new Status());
	}
}
