package cc.xulu.controller;

import cc.xulu.common.Status;
import com.jfinal.core.Controller;

import cc.xulu.model.*;

public class CommentController extends Controller {
	public void index() {
		setAttr("commentPage", Comment.dao.paginate(getParaToInt(0, 1), 10, "select *", "from comment order by id asc"));
		render("list.html");
	}
	
	public void add() {
	    setAttr("post", Post.dao.find("select * from post"));
	    setAttr("user", User.dao.find("select * from user"));
	    render("edit.html");
	}

    public void edit() {
        setAttr("post", Post.dao.find("select * from post"));
        setAttr("user", User.dao.find("select * from user"));
        setAttr("comment", Comment.dao.findById(getParaToInt()));
        render("edit.html");
    }
	
	public void save() {
		Comment comment = getModel(Comment.class);

		comment.save();

        renderJson(comment);
	}

    public void show() {
        Comment comment = Comment.dao.findById(getParaToInt());

        renderJson(comment);

    }
	
	public void update() {
        Comment comment = getModel(Comment.class);
        comment.update();
        renderJson(new Status());
	}
	
	public void delete() {
        Comment.dao.deleteById(getParaToInt());
        renderJson(new Status());
	}
}
